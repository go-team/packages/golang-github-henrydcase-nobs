# NOBS Crypto 

Cryptographic implementation of quantum-resistant primitives in Go.

## Licence

[WTFPLv2](https://en.wikipedia.org/wiki/WTFPL) except if specified differently in the subfolders.
